export const SIDEBAR = {
  en: [
    { text: 'Getting Started', header: true },
    { text: 'Introduction', link: 'en/introduction' },
    { text: 'Getting Started', link: 'en/getting-started' },
    { text: 'Example', link: 'en/example' },
  ],
};

export const SITE = {
  title: 'Astro Documentation',
  description: 'Build faster websites with less client-side Javascript.',
};

export const OPEN_GRAPH = {
  locale: 'en_US',
  image: {
    src: 'https://github.com/snowpackjs/astro/blob/main/assets/social/banner.png?raw=true',
    alt: 'astro logo on a starry expanse of space,' + ' with a purple saturn-like planet floating in the right foreground',
  },
  twitter: 'astrodotbuild',
};
